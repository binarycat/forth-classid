\ class datastructue:
\ [cell] min-id
\ [cell] max-id (this id is not part of the class, we are using a classic half-inclusive range)
\ [cell] prevclass-ptr
: id-in-class? ( id class -- bool )
  2@ swap within ;
: class-size 2@ - ;

\ returns 0 if input is 0, otherwise returns "max-id" field
\ : classid-max dup if cell+ @ then ;

: class: ( superclass prevclass minid -- superclass newclass newclass minid )
  create here >r \ superclass prevclass minid
  \ rot dup if cell+ cell+ ! else 2drop then \ superclass minid
  dup >r dup 2, \ superclass prevclass
  , r> r> dup rot ;

: id: ( class id -- class id+1 )
  dup constant 1+ ;

: class-end ( superclass thisclass prevclass id -- superclass prevclass id )
  rot 2dup cell+ ! drop ;

: skip-to-id ( id newid -- newid )
  tuck > abort" tried to skip backwards in ids" ;

: classlist-begin 0 0 0 ;
: classlist-end drop nip ;

: subclass? ( class superclass -- bool )
  2dup = if 2drop false exit then
  over @ over id-in-class? if
    over cell+ @ 1- over id-in-class? if
      2drop true exit
    then
  then
  2drop false ;

\ retrives the immediate superclass of a class (or 0 if none)
\ takes advantage of the specific order of the list (the first superclass we run into will always be the immediate superclass)
: find-superclass ( class -- superclass )
  dup begin \ class superclass
    dup while
    2dup subclass? 0= while
    cell+ cell+ @
  repeat then nip ;

\ takes advantage of the same property of ordering as "find-superclass",
\ where more specific classes come before their superclass
: find-class ( id classlist -- class )
  begin
    dup while
    2dup id-in-class? 0= while
    cell+ cell+ @
  repeat then nip ;
