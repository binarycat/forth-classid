require classid.fs
require test/ttester.fs

classlist-begin
  class: Animalia
    class: Felidae
      class: Panthera
        id: Panthera-pardus ( Leopard )
      class-end
      id: Felis-catus
    class-end
  class-end
  class: Protozoa
  class-end
classlist-end
constant taxonomy

testing classid words

T{ Felis-catus Felidae id-in-class? -> true }T
T{ Panthera-pardus Felidae id-in-class? -> true }T
T{ Felis-catus Animalia id-in-class? -> true }T
T{ Felidae class-size -> 2 }T
T{ Panthera class-size -> 1 }T

T{ Animalia find-superclass -> 0 }T
T{ Felidae find-superclass -> Animalia }T
T{ Panthera find-superclass -> Felidae }T
T{ Protozoa find-superclass -> 0 }T

T{ Panthera-pardus taxonomy find-class -> Panthera }T
T{ Felis-catus taxonomy find-class -> Felidae }T

T{ Panthera Animalia subclass? -> true }T


